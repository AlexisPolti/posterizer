#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Tile widget

@authors : Alexis Polti <alexis@polti.name> with help from
https://stackoverflow.com/questions/52926119/qgraphicsitem-interactive-resize-for-rectangles
"""

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import Qt, QObject
from commons import *
import math
import copy

__all__ = ["Tile"]

color_normal = Qt.blue
color_hover = Qt.green
color_selected = Qt.red


class Anchor(QtWidgets.QGraphicsRectItem):
    """
    Anchors used to resize tiles
    """

    def __init__(self, x, y, parent, anchorSize):
        super(Anchor, self).__init__(0, 0, anchorSize, anchorSize, parent)
        self.setPos(x - anchorSize / 2, y - anchorSize / 2)
        self.setPen(QtGui.QPen(color_normal))
        self.setBrush(QtGui.QBrush(color_normal))
        self.setFlags(
            QtWidgets.QGraphicsItem.ItemIsMovable
            | QtWidgets.QGraphicsItem.ItemIsFocusable
            | QtWidgets.QGraphicsItem.ItemSendsGeometryChanges
            | QtWidgets.QGraphicsItem.ItemSendsScenePositionChanges
        )
        self.setAcceptHoverEvents(True)
        self.setTransformOriginPoint(self.rect().center())

    def mouseMoveEvent(self, e):
        if e.buttons() & Qt.LeftButton:
            # Ensure the mouse never enlarge the scene
            if not self.scene().sceneRect().contains(e.scenePos()):
                return
            if self.parentItem().anchorMoved(self, e):
                super(Anchor, self).mouseMoveEvent(e)

    def hoverEnterEvent(self, e):
        super(Anchor, self).hoverEnterEvent(e)
        self.setPen(QtGui.QPen(color_hover))
        self.update()

    def hoverLeaveEvent(self, e):
        super(Anchor, self).hoverLeaveEvent(e)
        self.setPen(QtGui.QPen(color_normal))
        self.update()

    def mouseReleaseEvent(self, e):
        self.parentItem().anchorMoved(self, e)
        super(Anchor, self).mouseReleaseEvent(e)


class Tile(QtWidgets.QGraphicsRectItem):
    """
    A tile is composed of a red rectangle and four anchors used to resize it.
    """

    tiles = []

    def __init__(self, x, y, w, h, scale, tileType):
        super(Tile, self).__init__(QtCore.QRectF(x, y, w, h))

        # Parameters
        self.setFlags(
            QtWidgets.QGraphicsItem.ItemIsMovable
            | QtWidgets.QGraphicsItem.ItemIsFocusable
            | QtWidgets.QGraphicsItem.ItemSendsGeometryChanges
            | QtWidgets.QGraphicsItem.ItemSendsScenePositionChanges
        )
        self.setAcceptHoverEvents(True)

        # Internal variables
        self.type = tileType
        self.color = color_normal
        self.old_color = color_normal
        self.z = 0
        self.pen_width = 2 / scale
        self.anchorScale = 1
        self.anchorSize = 20 / scale
        self.hover = False
        self.anchor_tl = Anchor(x, y, self, self.anchorSize)
        self.anchor_tr = Anchor(x + w, y, self, self.anchorSize)
        self.anchor_bl = Anchor(x, y + h, self, self.anchorSize)
        self.anchor_br = Anchor(x + w, y + h, self, self.anchorSize)
        self.last_pos = QtCore.QPointF()
        self.justCreated = self.type == userTile
        self.number = 0
        self.number_left = None
        self.number_right = None
        self.number_top = None
        self.number_bottom = None
        self.selected = False

        self.updatePen()
        self.setAnchorsVisible(False)
        self.setZValue(self.z)
        self.tiles.append(self)

    def boundingRect(self):
        return self.rect().marginsAdded(
            QtCore.QMarginsF(
                self.anchorSize / 2,
                self.anchorSize / 2,
                self.anchorSize / 2,
                self.anchorSize / 2,
            )
        )

    def setScaleFactor(self, factor):
        # Update pen width
        self.pen_width /= factor
        self.updatePen()

        # Update anchor size
        self.anchorScale /= factor
        self.anchor_tl.setScale(self.anchorScale)
        self.anchor_tr.setScale(self.anchorScale)
        self.anchor_bl.setScale(self.anchorScale)
        self.anchor_br.setScale(self.anchorScale)
        self.update()

    def updatePen(self, color=None):
        if color is None:
            color = self.color
        self.setPen(QtGui.QPen(self.color, self.pen_width))

    def anchorMoved(self, anchor, e):
        self.prepareGeometryChange()

        if anchor == self.anchor_br or anchor == self.anchor_tl:
            rect = QtCore.QRectF(
                self.anchor_tl.pos()
                + QtCore.QPointF(self.anchorSize / 2, self.anchorSize / 2),
                self.anchor_br.pos()
                + QtCore.QPointF(self.anchorSize / 2, self.anchorSize / 2),
            ).normalized()

            self.anchor_bl.setPos(
                self.rect().bottomLeft()
                - QtCore.QPointF(self.anchorSize / 2, self.anchorSize / 2)
            )
            self.anchor_tr.setPos(
                self.rect().topRight()
                - QtCore.QPointF(self.anchorSize / 2, self.anchorSize / 2)
            )

        if anchor == self.anchor_bl or anchor == self.anchor_tr:
            rect = QtCore.QRectF(
                self.anchor_bl.pos()
                + QtCore.QPointF(self.anchorSize / 2, self.anchorSize / 2),
                self.anchor_tr.pos()
                + QtCore.QPointF(self.anchorSize / 2, self.anchorSize / 2),
            ).normalized()

            self.anchor_br.setPos(
                self.rect().bottomRight()
                - QtCore.QPointF(self.anchorSize / 2, self.anchorSize / 2)
            )
            self.anchor_tl.setPos(
                self.rect().topLeft()
                - QtCore.QPointF(self.anchorSize / 2, self.anchorSize / 2)
            )

        # Avoid overlaps
        if rect.left() > rect.right():
            return False
        if rect.top() > rect.bottom():
            return False

        self.setRect(rect)
        return True

    def setAnchorsVisible(self, state):
        self.anchor_bl.setVisible(state)
        self.anchor_br.setVisible(state)
        self.anchor_tl.setVisible(state)
        self.anchor_tr.setVisible(state)

    def hoverEnterEvent(self, e):
        self.setAnchorsVisible(True)
        if self.isSelected():
            self.color = color_selected
            self.old_color = color_hover
        else:
            self.old_color = self.color
            self.color = color_hover
        self.hover = True
        self.updatePen()
        self.update()

    def hoverLeaveEvent(self, e):
        self.setAnchorsVisible(False)
        if self.isSelected():
            self.color = color_selected
        else:
            self.color = color_normal
        self.old_color = color_normal
        self.updatePen()
        self.hover = False
        self.update()

    def mousePressEvent(self, e):
        self.last_pos = e.lastScreenPos()
        self.lastButtons = e.buttons()
        super(Tile, self).mousePressEvent(e)

    def mouseReleaseEvent(self, e):
        # If a tile was just created, then unselect it
        if self.justCreated:
            self.setSelectedTile(False)
            self.justCreated = False
            return

        pos = e.lastScreenPos() - self.last_pos
        distance = math.sqrt(math.pow(pos.x(), 2) + math.pow(pos.y(), 2))
        if (distance < 2) and (self.lastButtons & Qt.LeftButton):
            # If no modifier, then unselect all tiles and toggle active
            selected = [t for t in self.tiles if t.selected]
            if QtWidgets.QApplication.keyboardModifiers() == Qt.NoModifier:
                # Unselect other tiles
                for tile in self.tiles:
                    if tile != self:
                        tile.setSelectedTile(False)
                # Toggle self
                self.setSelectedTile(not self.selected)
            # If SHIFT modifier, then add current item to selected_tiles
            if QtWidgets.QApplication.keyboardModifiers() == Qt.ShiftModifier:
                if not self.selected:
                    self.setSelectedTile(True)
            # If CTRL modifier, then toggle active
            if QtWidgets.QApplication.keyboardModifiers() == Qt.ControlModifier:
                self.setSelectedTile(not self.selected)
            # Send a signal to notify main window that selection has changed
            signals.selectionChanged.emit()
        else:
            super(Tile, self).mouseReleaseEvent(e)

    def setSelectedTile(self, select):
        if select:
            self.selected = True
            self.old_color = self.color
            self.color = color_selected
            self.updatePen()
            self.update()
        else:
            self.selected = False
            self.color = self.old_color
            self.updatePen()
            self.update()

    def isSelected(self):
        return self.selected
