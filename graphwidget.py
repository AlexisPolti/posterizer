#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
GraphWidget widget

@authors : Alexis Polti <alexis@polti.name> with help from
"""

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import Qt, QObject
from PyQt5.QtCore import pyqtSignal
from tile import Tile
from commons import signals

__all__ = ["GraphWidget", "CreateTile"]


class GraphWidget(QtWidgets.QGraphicsView):

    # Create signals

    def __init__(self, parent):
        super(GraphWidget, self).__init__(parent)

        # Init parameters
        self.setTransformationAnchor(QtWidgets.QGraphicsView.AnchorUnderMouse)
        self.setResizeAnchor(QtWidgets.QGraphicsView.AnchorUnderMouse)

        # Init internal variables
        self.createTileMode = False
        self.pos = QtCore.QPointF()

    def wheelEvent(self, event):
        f = pow(2.0, event.angleDelta().y() / 240.0)
        self.scaleView(f)

    def scaleView(self, scaleFactor):
        factor = (
            self.transform()
            .scale(scaleFactor, scaleFactor)
            .mapRect(QtCore.QRectF(0, 0, 1, 1))
            .width()
        )
        if factor < 0.01 or factor > 20:
            return
        self.scale(scaleFactor, scaleFactor)

        # Update pen width
        for i in self.scene().items():
            if type(i) == Tile:
                i.setScaleFactor(scaleFactor)
                i.transform = self.transform()

    def mouseMoveEvent(self, e):
        self.pos = self.mapToScene(e.pos())
        super(GraphWidget, self).mouseMoveEvent(e)

    def mouseReleaseEvent(self, e):
        # If we were in drag mode, get out of it
        self.setDragMode(QtWidgets.QGraphicsView.NoDrag)
        self.enableTiles()
        # Forward event
        super(GraphWidget, self).mouseReleaseEvent(e)

    def mousePressEvent(self, e):
        """
        Dispatch mouse events
        """
        # On middle button, enable dragmode, and fake a left button event.
        if e.buttons() & Qt.MidButton:
            self.disableTiles()
            self.setDragMode(QtWidgets.QGraphicsView.ScrollHandDrag)

            ne = QtGui.QMouseEvent(
                QtCore.QEvent.GraphicsSceneMousePress,
                e.pos(),
                Qt.MouseButton.LeftButton,
                e.buttons(),
                Qt.KeyboardModifiers(),
            )
            super(GraphWidget, self).mousePressEvent(ne)

        # On create tiles mode
        if self.createTileMode and e.buttons() & Qt.LeftButton:
            pos = self.mapToScene(e.pos())
            signals.createTile.emit(pos)
            self.enableTiles()
        super(GraphWidget, self).mousePressEvent(e)

    def disableTiles(self):
        for i in self.scene().items():
            i.setAcceptedMouseButtons(Qt.NoButton)
            i.setAcceptHoverEvents(False)

    def enableTiles(self):
        for i in self.scene().items():
            i.setAcceptedMouseButtons(Qt.AllButtons)
            i.setAcceptHoverEvents(True)

    def createTile(self):
        self.createTileMode = True
        self.disableTiles()
