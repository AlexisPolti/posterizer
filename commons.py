#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
List of common objects used by multiple modules:
  - pyQt signals
  - exported constants
  - ...
"""

from PyQt5 import QtCore
from PyQt5.QtCore import QObject, pyqtSignal


class Signals(QObject):
    selectionChanged = pyqtSignal()
    createTile = pyqtSignal(object)


signals = Signals()

autoTile = 0
userTile = 1

# Settings
QtCore.QCoreApplication.setOrganizationName("Ktatruc")
QtCore.QCoreApplication.setOrganizationDomain("Ktatruc")
QtCore.QCoreApplication.setApplicationName("Poster")
settings = QtCore.QSettings()
