
FILES =  Ui_main_window.py ressources_rc.py

all: $(FILES)

Ui_%.py: %.ui
	pyuic5 -x $< -o $@

ressources_rc.py: res/ressources.qrc
	pyrcc5 $< -o $@

clean:
	rm -rf $(FILES) __pycache__ output_*

.PHONY: clean all
