#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Main window code

@authors : Alexis Polti <alexis@polti.name>
"""

import sys
import os
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import Qt, QObject
from PyQt5.QtWidgets import QApplication, QColorDialog
from Ui_main_window import Ui_main_window
from tile import Tile
import math
from commons import *

__all__ = ["MW"]

a4_ratio = 21 / 29.7
HORIZONTAL = 0
VERTICAL = 1
ROW = 0
COL = 1


class MW(QtWidgets.QMainWindow):
    """
    This class is the main window
    """

    # Methods
    def __init__(self, parent=None):
        super(MW, self).__init__(parent)
        self.map = None
        # Init UI
        self._initUI()

    def _initUI(self):
        self.ui = Ui_main_window()
        self.ui.setupUi(self)
        window_size = settings.value("window_size")
        if window_size is not None:
            self.resize(window_size)

        # Internal variables
        self.filename = None
        self.w_map = 0
        self.h_map = 0
        self.xtiles = 0
        self.ytiles = 0
        self.tiles = []
        self.fit = ROW
        self.nfit = 0
        self.h_overlap = 0.15
        self.v_overlap = 0.15
        self.page_orientation = HORIZONTAL
        self.output_prefix = settings.value("outputname_prefix", "tile_")
        self.output_dir = settings.value("output_dir", QtCore.QDir.currentPath())
        self.dirty = False
        self.createdTile = None
        self.arrowColor = settings.value("arrowColor", QtGui.QColor("Red"))
        self.fontSize = int(settings.value("fontSize", 20))

        # Set view
        self.view = self.ui.gw

        # Create scene
        self.scene = QtWidgets.QGraphicsScene()
        self.view.setScene(self.scene)

        # Prepare widgets
        self.ui.output_dir_lineEdit.setText(self.output_dir)
        self.ui.filename_lineEdit.setText(self.output_prefix)
        self.ui.line_row_comboBox.addItem("line")
        self.ui.line_row_comboBox.addItem("column")
        self.ui.line_row_comboBox.setCurrentText(settings.value("line_row", "line"))
        self.ui.number_size_spinBox.setValue(self.fontSize)
        self.ui.number_fontComboBox.setCurrentFont(
            settings.value("font", QtGui.QFont("Arial"))
        )
        self.ui.bold_checkBox.setChecked(settings.value("bold", "true") == "true")
        self.ui.italic_checkBox.setChecked(settings.value("italic", "false") == "true")
        self.ui.include_number_checkBox.setChecked(
            settings.value("include_page_number", "true") == "true"
        )
        self.ui.colorButton.setStyleSheet("background-color:" + self.arrowColor.name())
        self.filenameLabel = QtWidgets.QLabel(self)
        self.ui.statusbar.addWidget(self.filenameLabel, 1)
        self.infoLabel = QtWidgets.QLabel(self)
        self.ui.statusbar.addPermanentWidget(self.infoLabel, 0)
        self.ui.ntiles_spinBox.setValue(int(settings.value("ntiles", 3)))
        self.ui.h_overlap_spinBox.setValue(int(settings.value("h_overlap", 15)))
        self.ui.v_overlap_spinBox.setValue(int(settings.value("v_overlap", 15)))

        # Connect signals
        self.ui.action_open_file.triggered.connect(self.handle_open_file)
        self.ui.action_exit.triggered.connect(self.close)
        self.ui.action_export_tiles.triggered.connect(self.exportTiles)
        self.ui.action_delete_selected_tiles.triggered.connect(self.deleteSelectedTiles)
        self.ui.action_create_tile.triggered.connect(self.view.createTile)
        self.ui.action_close_file.triggered.connect(self.closeFile)
        self.ui.actionDelete_all_tiles.triggered.connect(self.deleteAllTiles)
        self.ui.actionDelete_automatic_tiles.triggered.connect(self.deleteAutoTiles)
        self.ui.actionDelete_user_tiles.triggered.connect(self.deleteUserTiles)
        self.ui.action_raise_selection.triggered.connect(self.raiseSelection)
        self.ui.action_lower_selection.triggered.connect(self.lowerSelection)
        self.ui.action_move_selection_to_top.triggered.connect(self.moveSelectionToTop)
        self.ui.action_move_selection_to_bottom.triggered.connect(
            self.moveSelectionToBottom
        )

        self.ui.tileButton.pressed.connect(self.autoTile)
        self.ui.ntiles_spinBox.valueChanged.connect(self.autoTile)
        self.ui.line_row_comboBox.currentIndexChanged.connect(self.autoTile)
        self.ui.h_overlap_spinBox.valueChanged.connect(self.autoTile)
        self.ui.v_overlap_spinBox.valueChanged.connect(self.autoTile)
        self.ui.horizontal_radioButton.toggled.connect(self.autoTile)
        self.ui.colorButton.pressed.connect(self.changeArrowColor)

        self.ui.filename_lineEdit.textChanged.connect(self.filenameChanged)
        self.ui.outputDirButton.clicked.connect(self.chooseOutputDirectory)
        signals.selectionChanged.connect(self.selectionChanged)
        self.view.scene().changed.connect(self.view.updateScene)
        signals.createTile.connect(self.createTile)

        if os.path.exists("grs.jpg"):
            self.read_image("grs.jpg")

        self.updateUI()

    def saveSettings(self):
        settings.setValue("output_dir", self.output_dir)
        settings.setValue("outputname_prefix", self.ui.filename_lineEdit.text())
        settings.setValue("window_size", self.size())
        settings.setValue("font", self.ui.number_fontComboBox.currentFont())
        settings.setValue("bold", self.ui.bold_checkBox.isChecked())
        settings.setValue("italic", self.ui.italic_checkBox.isChecked())
        settings.setValue("arrowColor", self.arrowColor)
        settings.setValue("fontSize", self.ui.number_size_spinBox.value())
        settings.setValue("line_row", self.ui.line_row_comboBox.currentText())
        settings.setValue("ntiles", self.ui.ntiles_spinBox.value())
        settings.setValue("h_overlap", self.ui.h_overlap_spinBox.value())
        settings.setValue("v_overlap", self.ui.v_overlap_spinBox.value())
        settings.setValue(
            "include_page_number", self.ui.include_number_checkBox.isChecked()
        )
        settings.sync()

    def closeEvent(self, event):
        if self.dirty:
            reply = QtWidgets.QMessageBox.question(
                self,
                "Message",
                "Are you sure you want to quit? Any unsaved / non-exported work will be lost.",
                QtWidgets.QMessageBox.Close | QtWidgets.QMessageBox.Cancel,
                QtWidgets.QMessageBox.Cancel,
            )

            if reply == QtWidgets.QMessageBox.Close:
                event.accept()
            else:
                event.ignore()
        self.saveSettings()
        event.accept()

    def closeFile(self):
        if self.dirty:
            reply = QtWidgets.QMessageBox.question(
                self,
                "Message",
                "Are you sure you want to quit? Any unsaved / non-exported work will be lost.",
                QtWidgets.QMessageBox.Close | QtWidgets.QMessageBox.Cancel,
                QtWidgets.QMessageBox.Cancel,
            )

            if reply == QtWidgets.QMessageBox.Cancel:
                return
        self.deleteTiles()
        self.scene.removeItem(self.map)
        self.map = None
        self.updateUI()
        self.dirty = False

    def handle_open_file(self):
        path, _ = QtWidgets.QFileDialog.getOpenFileName(
            self, "Open File", ".", "Image Files (*.png *.jpg *.bmp)", None,
        )
        if not path:
            return

        self.read_image(path)

    def read_image(self, filename):
        self.deleteTiles()
        # Create new scene and reconnect signals
        self.scene = QtWidgets.QGraphicsScene()
        self.view.setScene(self.scene)
        self.view.scene().selectionChanged.connect(self.selectionChanged)
        self.view.scene().changed.connect(self.view.updateScene)
        self.view.scene().changed.connect(self.zReorderTiles)
        self.view.scene().changed.connect(self.sortTiles)

        self.map = QtWidgets.QGraphicsPixmapItem(
            QtGui.QPixmap.fromImage(QtGui.QImage(filename))
        )
        self.add_map_to_scene()
        self.filename = filename
        self.updateUI()
        self.dirty = False

    def add_map_to_scene(self):
        self.scene.addItem(self.map)
        self.w_map, self.h_map = (
            self.map.pixmap().width(),
            self.map.pixmap().height(),
        )
        self.map.setZValue(-1)
        self.zoom_all()
        self.filenameLabel.setText(
            ("file = {}, width = {}, height = {}").format(
                self.filename, self.w_map, self.h_map
            )
        )

    def wheel_event(self, event):
        steps = event.angleDelta().y() / 120.0
        self.zoom(steps)
        event.accept()

    def zoom(self, steps):
        self.view.scale(1 + 0.2 * steps, (1 + 0.2 * steps))

    def zoom_all(self):
        self.view.fitInView(self.scene.sceneRect(), Qt.KeepAspectRatio)

    def showEvent(self, event):
        self.view.fitInView(self.scene.sceneRect(), Qt.KeepAspectRatio)

    def deleteTiles(self, tiles=None):
        """
        Remove tiles.
        If tiles is None, remove all tiles.
        """
        if tiles is None:
            for t in self.tiles:
                self.scene.removeItem(t)
                t.tiles.clear()
            self.tiles.clear()
        else:
            for t in tiles:
                self.scene.removeItem(t)
                self.tiles.remove(t)
                t.tiles.remove(t)
        # Update UI
        self.updateUI()

    def deleteSelectedTiles(self):
        selected = [t for t in self.tiles if t.isSelected()]
        self.deleteTiles(selected)

    def deleteAutoTiles(self):
        autotiles = [t for t in self.tiles if t.type == autoTile]
        self.deleteTiles(autotiles)

    def deleteUserTiles(self):
        usertiles = [t for t in self.tiles if t.type == userTile]
        self.deleteTiles(usertiles)

    def deleteAllTiles(self):
        self.deleteTiles()

    def updateUI(self):
        """
        Update icons regarding selected tiles, aso…
        """
        selected = [t for t in self.tiles if t.isSelected()]
        autotiles = [t for t in self.tiles if t.type == autoTile]
        usertiles = [t for t in self.tiles if t.type == userTile]
        if self.nfit != 0:
            if self.fit == ROW:
                self.xtiles = self.nfit
                self.ytiles = len(autotiles) // self.nfit
            else:
                self.ytiles = self.nfit
                self.xtiles = len(autotiles) // self.nfit

        self.ui.action_delete_selected_tiles.setEnabled(len(selected) > 0)
        self.ui.action_export_tiles.setEnabled(len(self.tiles) > 0)
        self.ui.tileButton.setEnabled(self.map != None)
        self.ui.action_close_file.setEnabled(self.map != None)
        self.ui.action_create_tile.setEnabled(self.map != None)
        self.ui.actionDelete_all_tiles.setEnabled(len(self.tiles) > 0)
        self.ui.actionDelete_automatic_tiles.setEnabled(len(autotiles) > 0)
        self.ui.actionDelete_user_tiles.setEnabled(len(usertiles) > 0)
        self.ui.action_raise_selection.setEnabled(len(selected) > 0)
        self.ui.action_lower_selection.setEnabled(len(selected) > 0)
        self.ui.action_move_selection_to_top.setEnabled(len(selected) > 0)
        self.ui.action_move_selection_to_bottom.setEnabled(len(selected) > 0)
        self.infoLabel.setText(
            "%d tile, %d per row, %d per colums, %d added by user"
            % (len(self.tiles), self.xtiles, self.ytiles, len(usertiles),)
        )
        self.filenameLabel.setText(
            "File:%s, x: %d pixels, y: %d pixels"
            % (self.filename, self.w_map, self.h_map)
        )

    def selectionChanged(self):
        # Sort and numerize tiles
        self.zReorderTiles()
        self.sortTiles()

        self.updateUI()

    def autoTile(self):
        if self.map == None:
            return
        if self.w_map == 0 or self.h_map == 0:
            return
        self.dirty = True

        # Delete automatic tiles
        self.deleteAutoTiles()

        self.nfit = int(self.ui.ntiles_spinBox.text())

        if self.ui.line_row_comboBox.currentText() == "line":
            self.fit = ROW
        else:
            self.fit = COL

        self.v_overlap = self.ui.v_overlap_spinBox.text().strip("%")
        self.v_overlap = float(self.v_overlap) / 100.0

        self.h_overlap = self.ui.h_overlap_spinBox.text().strip("%")
        self.h_overlap = float(self.h_overlap) / 100.0

        if self.ui.horizontal_radioButton.isChecked():
            self.page_orientation = HORIZONTAL
        elif self.ui.vertical_radioButton.isChecked():
            self.page_orientation = VERTICAL

        # Create tiles
        x = 0
        y = 0
        h = 0
        w = 0
        scale = self.view.transform().mapRect(QtCore.QRectF(0, 0, 1, 1)).width()

        if self.fit == ROW:
            w = math.ceil(
                float(self.w_map)
                / (self.nfit - self.nfit * self.h_overlap + self.h_overlap)
            )

            if self.page_orientation == HORIZONTAL:
                h = w * a4_ratio
            else:
                h = w / a4_ratio

        if self.fit == COL:
            h = math.ceil(
                float(self.h_map)
                / (self.nfit - self.nfit * self.v_overlap + self.v_overlap)
            )

            if self.page_orientation == HORIZONTAL:
                w = h / a4_ratio
            else:
                w = h * a4_ratio

        if self.fit == ROW:
            x = 0
            y = 0
            while (y + h * self.v_overlap) < self.h_map + 1:
                for n in range(self.nfit):
                    t = Tile(x, y, w, h, scale, autoTile)
                    self.tiles.append(t)
                    self.scene.addItem(t)
                    x += w * (1 - self.h_overlap)
                y += h * (1 - self.v_overlap)
                x = 0

        if self.fit == COL:
            x = 0
            y = 0
            while (x + w * self.h_overlap) < self.w_map + 1:
                for n in range(self.nfit):
                    t = Tile(x, y, w, h, scale, autoTile)
                    self.tiles.append(t)
                    self.scene.addItem(t)
                    y += h * (1 - self.v_overlap)
                x += w * (1 - self.h_overlap)
                y = 0

            self.xtiles = self.nfit
            self.ytiles = self

        # Sort and numerize tiles
        self.zReorderTiles()
        self.sortTiles()

        # Update UI
        self.updateUI()

    def sortTiles(self):
        """Sort tiles: auto tiles first, ordered horizontaly first, then
        user added tiles, then tiles outside the map (which won't be exported)
        """
        if self.nfit == 0:
            return

        autotiles = [
            t for t in self.tiles if t.type == autoTile and self.map.collidesWithItem(t)
        ]
        usertiles = [
            t for t in self.tiles if t.type == userTile and self.map.collidesWithItem(t)
        ]
        othertiles = [
            t for t in self.tiles if t not in autotiles and t not in usertiles
        ]

        for i, tile in enumerate(autotiles):
            tile.number = i + 1

        x, y = 1, 1

        if self.fit == ROW:
            xmax = self.nfit
            ymax = len(autotiles) / self.nfit
        else:
            ymax = self.nfit
            xmax = len(autotiles) / self.nfit

        for tile in autotiles:
            tile.number_top = tile.number - self.nfit
            if tile.number_top < 1:
                tile.number_top = 0

            tile.number_bottom = tile.number + self.nfit
            if y >= ymax:
                tile.number_bottom = 0

            tile.number_left = tile.number - 1
            if (tile.number_left % self.nfit) < 1:
                tile.number_left = 0

            tile.number_right = tile.number + 1
            if x >= xmax:
                tile.number_right = 0

            x += 1
            if x > xmax:
                x = 1
                y += 1

        usertiles.sort(
            key=lambda tile: (
                tile.mapToScene(tile.rect().topLeft()).y(),
                tile.mapToScene(tile.rect().topLeft()).x(),
            )
        )
        for tile in usertiles:
            tile.number = None
            tile.number_left = None
            tile.number_right = None
            tile.number_top = None
            tile.number_bottom = None

        self.tiles = autotiles + usertiles + othertiles

    def exportTiles(self):
        """
        Export tiles as png
        """
        # Sort and numerize tiles
        self.zReorderTiles()
        self.sortTiles()

        # Build and check filename
        ndigits = len("%d" % (len(self.tiles),))
        template = os.path.join(
            self.output_dir.strip(),
            self.ui.filename_lineEdit.text().strip() + "%0" + "%d" % ndigits + "d.png",
        )

        # Ensure every output files are valid and don't exit
        for i in range(len(self.tiles)):
            filename = template % (i + 1,)
            if os.path.exists(filename):
                ret = QtWidgets.QMessageBox.question(
                    self,
                    "Poster",
                    "The file %s already exists.\n"
                    "Do you want to overwrite it?" % filename,
                    QtWidgets.QMessageBox.Yes
                    | QtWidgets.QMessageBox.YesToAll
                    | QtWidgets.QMessageBox.Cancel,
                    QtWidgets.QMessageBox.Cancel,
                )
                if ret == QtWidgets.QMessageBox.YesToAll:
                    ret = QtWidgets.QMessageBox.Yes
                    break
                if ret == QtWidgets.QMessageBox.Cancel:
                    return

        # For each tile, get its rectangle position
        map = self.map.pixmap()
        QApplication.setOverrideCursor(Qt.WaitCursor)
        for i, t in enumerate(self.tiles):
            # Keep only tiles inside the map
            if not self.map.collidesWithItem(t):
                continue

            # Extract pixmap
            tl = t.mapToScene(t.rect().topLeft())
            br = t.mapToScene(t.rect().bottomRight())
            p = map.copy(QtCore.QRect(tl.toPoint(), br.toPoint()))

            # Add numbering if necessary
            if self.ui.include_number_checkBox.isChecked():
                self.addNumbering(t, p)

            # Export file
            filename = template % (i + 1,)
            p.save(filename, "png")
        QApplication.restoreOverrideCursor()
        self.dirty = False

    def orderTilesBySurface(self):
        self.tiles.sort(
            key=lambda tile: tile.rect().width() * tile.rect().height(), reverse=True
        )

    def zReorderTiles(self):
        self.tiles.sort(key=lambda tile: tile.z,)

        i = 1
        for tile in self.tiles:
            tile.z = i
            tile.setZValue(i)
            i += 1

    def chooseOutputDirectory(self):
        directory = str(QtWidgets.QFileDialog.getExistingDirectory())
        if directory != "":
            self.ui.output_dir_lineEdit.setText(directory)
            self.output_dir = directory

    def filenameChanged(self):
        settings.setValue("outputname_prefix", self.ui.filename_lineEdit.text())

    def createTile(self, pos):
        scale = self.view.transform().mapRect(QtCore.QRectF(0, 0, 1, 1)).width()
        t = Tile(pos.x(), pos.y(), 300, 300, scale, userTile)
        self.tiles.append(t)
        self.scene.addItem(t)
        self.ui.action_create_tile.setChecked(False)
        self.dirty = True
        self.view.createTileMode = False
        self.zReorderTiles()
        self.updateUI()

    def addNumbering(self, tile, pixmap):
        # Strip out non numerable tiles
        if tile.type != autoTile:
            return

        tl = pixmap.rect().topLeft()
        h = pixmap.rect().height()
        w = pixmap.rect().width()

        # Create a painter
        painter = QtGui.QPainter(pixmap)
        painter.setRenderHints(QtGui.QPainter.Antialiasing)

        # Select font
        font = QtGui.QFont(self.ui.number_fontComboBox.currentFont())
        font.setStyleStrategy(
            QtGui.QFont.PreferAntialias
            | QtGui.QFont.PreferQuality
            | QtGui.QFont.ForceOutline
        )
        font.setBold(self.ui.bold_checkBox.isChecked())
        font.setItalic(self.ui.italic_checkBox.isChecked())

        # Set point size
        pointSize = font.pointSize() * max(w, h) / 10000
        pointSize = pointSize * self.ui.number_size_spinBox.value()
        font.setPointSizeF(pointSize)
        painter.setFont(font)
        metrics = QtGui.QFontMetricsF(font)

        pen = QtGui.QPen()
        pen.setWidth(1)
        pen.setColor(self.arrowColor)
        painter.setPen(pen)

        # Display tile number
        text = "Tile %d" % (tile.number,)
        br = metrics.boundingRect(text)
        position = tl + QtCore.QPointF(w / 100, br.height())
        br.moveTo(position)
        br.translate(0, -br.height())
        painter.drawText(br, Qt.AlignCenter, text)

        # Display pages arrow
        br = metrics.boundingRect("XX")
        arrowWidth = br.width()
        br.setWidth(arrowWidth)

        if tile.number_left != 0:
            text = "%d" % tile.number_left

            arrow = QtGui.QPixmap(":/icons/arrow-left.png")
            arrow = self.changePixmapColor(arrow, self.arrowColor)
            arrow = arrow.scaledToWidth(int(arrowWidth), Qt.SmoothTransformation)

            pos = tl
            pos += QtCore.QPointF(0, h / 2)

            pos += QtCore.QPointF(10, 0)
            painter.drawPixmap(pos, arrow)

            br.moveTo(pos)
            br.translate(0, -br.height())
            painter.drawText(br, Qt.AlignCenter, text)

        if tile.number_right != 0:
            text = "%d" % tile.number_right

            arrow = QtGui.QPixmap(":/icons/arrow-right.png")
            arrow = self.changePixmapColor(arrow, self.arrowColor)
            arrow = arrow.scaledToWidth(int(arrowWidth), Qt.SmoothTransformation)

            pos = tl
            pos += QtCore.QPointF(w - br.width(), h / 2)

            pos += QtCore.QPointF(-10, 0)
            painter.drawPixmap(pos, arrow)

            br.moveTo(pos)
            br.translate(0, -br.height())
            painter.drawText(br, Qt.AlignCenter, text)

        if tile.number_top != 0:
            text = "%d" % tile.number_top

            arrow = QtGui.QPixmap(":/icons/arrow-top.png")
            arrow = self.changePixmapColor(arrow, self.arrowColor)
            arrow = arrow.scaledToHeight(int(arrowWidth), Qt.SmoothTransformation)

            pos = tl
            pos += QtCore.QPointF(w / 2 - br.width(), 0)

            painter.drawPixmap(pos, arrow)

            br.moveTo(pos)
            br.translate(br.width() / 2, 0)
            painter.drawText(br, Qt.AlignCenter, text)

        if tile.number_bottom != 0:
            text = "%d" % tile.number_bottom

            arrow = QtGui.QPixmap(":/icons/arrow-bottom.png")
            arrow = self.changePixmapColor(arrow, self.arrowColor)
            arrow = arrow.scaledToHeight(int(arrowWidth), Qt.SmoothTransformation)

            pos = tl
            pos += QtCore.QPointF(w / 2 - br.width(), h - arrowWidth)

            painter.drawPixmap(pos, arrow)

            br.moveTo(pos)
            br.translate(br.width() / 2, 0)
            painter.drawText(br, Qt.AlignCenter, text)

        painter.end()

    def raiseSelection(self):
        self.zReorderTiles()
        selected = [t for t in self.tiles if t.isSelected()]
        for tile in selected:
            tile.z = tile.z + 1
            tile.setZValue(tile.z)
        self.zReorderTiles()

    def moveSelectionToTop(self):
        self.zReorderTiles()
        selected = [t for t in self.tiles if t.isSelected()]
        z = 0
        for tile in self.tiles:
            z = max(z, tile.z)
        z += 1
        for tile in selected:
            tile.z = z
            tile.setZValue(z)
            z += 1
        self.zReorderTiles()

    def lowerSelection(self):
        self.zReorderTiles()
        selected = [t for t in self.tiles if t.isSelected()]
        for tile in selected:
            tile.z = tile.z - 1
            tile.setZValue(tile.z)
        self.zReorderTiles()

    def moveSelectionToBottom(self):
        self.zReorderTiles()
        selected = [t for t in self.tiles if t.isSelected()]
        z = 0
        for tile in selected:
            tile.z = z
            tile.setZValue(z)
            z -= 1
        self.zReorderTiles()

    def changePixmapColor(self, pixmap, color):
        tmp = pixmap.toImage()

        for y in range(tmp.height()):
            for x in range(tmp.width()):
                color.setAlpha(tmp.pixelColor(x, y).alpha())
                tmp.setPixelColor(x, y, color)

        pixmap = QtGui.QPixmap.fromImage(tmp)
        color.setAlpha(255)
        return pixmap

    def changeArrowColor(self):
        color = QtWidgets.QColorDialog()
        color.setOption(QtWidgets.QColorDialog.ShowAlphaChannel)
        color = color.getColor()
        if not color.isValid():
            return
        self.ui.colorButton.setStyleSheet("background-color:" + color.name())
        self.arrowColor = QtGui.QColor(color)


# Problem: when exporting tiles which ahev moved, there is a problem
# Problem2 : the UI become unresponsive soùe random times (generaly after
# exporting images)x
