# Posterizer

## Introduction

This is a tool to split a large image (say for example a big map) into smaller images that fit on standard A4 pages (only this format is supported for now)

- Automatic tiling of the map
- You can specify the overlap between pages
- You can specify the orientation of the pages (portrait or landscape)
- You can specify the number of page per row or column
- You can add your own tile (the format is free)


## Installation and usage

You have to install `PyQt5`, which is provided by **python-pyqt5** in ArchLinux. Then just launch `./poster.py`

If you want to work in a virtualenv, a `requirements.txt` file is provided.

If it says that `Ui_main_window.py` or `ressources_rc.py` is missing (which it shouldn't), then install `qt5 designer` (provided by **qt5-tools** on ArchLinux and type `make` to generate the missing files.

The UI should be self-explanatory :-)

Tiles selection:
- Select just one tile (and deselect others): click on it.
- Toggle the selection state of a tile without changing the others: Ctrl-click.
- Add tile to slection : Shift + click.

You can drag the view by clicking and dragging with the middle button.

## Bugs / missing features

This is a Work In Progress. There are a lot a features missing and some bugs. Please don't hesitate to file an [Issue](https://gitlab.com/AlexisPolti/posterizer/-/issues/new) or submit a [PR](https://gitlab.com/AlexisPolti/posterizer/-/merge_requests/new)!

Icons are ugly, I know. Artists, [PR](https://gitlab.com/AlexisPolti/posterizer/-/merge_requests/new) are welcome!
