#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os
from PyQt5 import QtCore, QtGui, QtWidgets
from main_window import MW

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    QtCore.QCoreApplication.setOrganizationName("Ktatruc")
    QtCore.QCoreApplication.setOrganizationDomain("Ktatruc")
    QtCore.QCoreApplication.setApplicationName("Poster")

    mw = MW()
    mw.show()
    if (sys.flags.interactive != 1) or not hasattr(QtCore, "PYQT_VERSION"):
        app.exec_()
